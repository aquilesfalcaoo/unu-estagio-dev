# SELEÇÃO DE ESTÁGIO EM DESENVOLVIMENTO WEB #

# PROJETO #

Desenvolva um mini sistema de gerenciamento acadêmico, o sistema deverá conter:

# Login de autenticação: #
  * Username ou E-mail;
  * Senha;


# Cadastro, Alteração, Remoção e Detalhamento de: #
  * Alunos;
  * Usuários;


Os alunos irão possuir: id, nome, data de nascimento;

Os usuários irão possuir: id, nome, username, e-mail, senha;

# INFORMAÇÕES TÉCNICAS #

* O projeto deverá ser desenvolvido utilizando PHP para o backend e Mysql para o banco de dados;

* O repositório deverá conter todos os códigos e artefatos desenvolvidos para o projeto;

* O repositório deverá conter o script de criação das tabelas do banco de dados;

# DESEJAVEIS (NÃO OBRIGATÓRIO)#

* Criação de API para o back-end;

* Utilização de FrameWork Javascript para o front-end;

* Arquitetura de Camadas;

* Testes unitários;

# RECOMENDAÇÕES (NÃO OBRIGATÓRIO) #

* Usar o Laravel para criação do projeto;
* Usar o [Slim Framework] para desenvolvimento da API back-end (https://www.slimframework.com/);
* Usar AngularJS para desenvolvimento do front-end;
* Escreva um README no seu projeto descrevendo as tecnologias usadas e por que foram escolhidas;
* Escreve um README no seu projeto descrevendo como rodar o seu projeto;

# AVALIAÇÃO #

Serão avaliados os seguintes aspectos:

  * Lógica de desenvolvimento;
  * Organização e Estrutura do código;
  * Modelagem do Banco de Dados;

# INSTRUÇÕES #

* Você deverá realizar um fork deste repositório e desenvolver todo o seu projeto no SEU repositório;

* O repositório deverá ser público para que possamos realizar a correção do teste;

* Os projetos deverão ser enviados via PULL REQUEST para este repositório;

# ATENÇÃO #

* Não se deve tentar fazer o PUSH diretamente para ESTE repositório!

